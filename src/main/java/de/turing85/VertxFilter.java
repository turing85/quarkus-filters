package de.turing85;

import io.quarkus.vertx.web.RouteFilter;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@Slf4j
public class VertxFilter {

 private final String correlationIdHeaderName;

  public VertxFilter(
      @ConfigProperty(name = "http.header.correlation.name") String correlationIdHeaderName) {
    this.correlationIdHeaderName = correlationIdHeaderName;
  }

  @RouteFilter(Integer.MAX_VALUE)
  public void addFilter (RoutingContext context) {
    processRequest(context.request());
    processResponse(context.request(), context.response());
    context.next();
  }

  private void processRequest(HttpServerRequest request) {
    addCorrelationIdIfNotPresent(request);
    logHeaders(request);
  }

  private void addCorrelationIdIfNotPresent(HttpServerRequest request) {
    final String providedCorrelationId = request.headers().get(correlationIdHeaderName);
    if (Objects.isNull(providedCorrelationId) || providedCorrelationId.isBlank()) {
      final String generatedCorrelationIdString = UUID.randomUUID().toString();
      log.info("CorrelationId was generated (\"{}\")", generatedCorrelationIdString);
      request.headers().add(correlationIdHeaderName, generatedCorrelationIdString);
    } else {
      log.info("CorrelationId was provided (\"{}\")", providedCorrelationId);
    }
  }

  private static void logHeaders(HttpServerRequest request) {
    logHeaders(request.headers(), "Request");
  }

  private static void logHeaders(MultiMap headers, String prefix) {
    final List<Entry<String, String>> entries = headers.entries();
    final int size = entries.size();
    final String lineSeparator = System.lineSeparator();
    final StringBuilder builder =
        new StringBuilder(String.format("%s-Headers:", prefix)).append(lineSeparator);
    for (int index = 0; index < size; ++index) {
      final Entry<String, String> entry = entries.get(index);
      builder.append(String.format("%s:%s", entry.getKey(), entry.getValue()));
      if (index < size - 1) {
        builder.append(lineSeparator);
      }
    }
    log.info(builder.toString());
  }

  private void processResponse(HttpServerRequest request, HttpServerResponse response) {
    response.putHeader(
        correlationIdHeaderName,
        Objects.requireNonNull(request.getHeader(correlationIdHeaderName)));
    response.headersEndHandler(aVoid -> logHeaders(response));
  }

  private static void logHeaders(HttpServerResponse response) {
    logHeaders(response.headers(), "Response");
  }
}