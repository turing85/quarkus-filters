# quarkus-filters Project

This project shows how to implement a HTTP filter that does the following things:
- Analyze all incoming HTTP requests whether they have a correlation id header. The header name for 
  the correlation ids is defined through property `http.header.correlation.name`. It defaults to 
  `X-Request-ID` and can be overwritten by:
  - setting environment variable `HTTP_HEADER_CORRELATION_NAME`, or
  - launching the `jar` with `-Dhttp.header.correlation.name=...`
- Log incoming and outgoing headers. The incoming headers are logged as early as possible, such 
  that we log the information send over the wire. Likewise, the outgoing headers are logged as late
  as possible.
 
## Building the application
Maven wrappers are included in the project, thus the application can be started in dev mode by 
executing
```shell
# Linux:  
./mvnw quarkus:dev

# Windows:
mvnw quarkus:dev
```
from the project's root.

## Generating some logs
Once the application is started, we can make some HTTP requests through [`cURL`][curl]:
```shell
curl -v http://localhost:8080/hello
```

The response should look similar to this:
```
*   Trying ::1...
* TCP_NODELAY set
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /hello HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.55.1
> Accept: */*
>
< HTTP/1.1 201 Created
< X-Request-ID: c06ac1a2-21fd-4ebc-8bdf-320f307a798f
< Content-Type: text/plain;charset=UTF-8
< Location: http://localhost:8080/foobar
< content-length: 14
<
Hello RESTEasy* Connection #0 to host localhost left intact
```

We can already see that the response header includes a `X-Request-ID`.

Taking a look at the log entry generated for the response headers:
```
...
2021-11-11 21:34:00,045 INFO  [de.tur.VertxFilter] (vert.x-eventloop-thread-4) Response-Headers:
X-Request-ID:foobar
accept-ranges:bytes
content-length:5359
date:Thu, 11 Nov 2021 20:34:00 GMT
content-type:text/html;charset=UTF-8
...
```
we can also see that headers for content length and location are included in the logs.

When we send a request with the `X-Request-ID` header set:
```shell
curl -v -H X-Request-ID:foobar http://localhost:8080
```

we see that the response includes the correlation id we passed in with the request:
```
*   Trying ::1...
* TCP_NODELAY set
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /hello HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.55.1
> Accept: */*
> X-Request-ID:foobar
>
< HTTP/1.1 201 Created
< X-Request-ID: foobar
< Content-Type: text/plain;charset=UTF-8
< Location: http://localhost:8080/foobar
< content-length: 14
<
Hello RESTEasy* Connection #0 to host localhost left intact
```

[curl]: https://curl.se/